#ifndef MESH
#define MESH

#include "triangle.h"

enum ChildType { e_hierarchy, e_triangles }; // Really should rename these.

typedef struct MeshHierarchy {
	enum ChildType child_type;
	size_t num_children;
	Vector3 aabb_top;
	Vector3 aabb_bottom;

	union {
		struct MeshHierarchy *mesh_hierarchy; // Not sure why we need "struct" here.
		struct Triangle *triangles;
	};
} MeshHierarchy;

#endif
