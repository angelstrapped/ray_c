#ifndef CAMERA
#define CAMERA

#include "vector3.h"

typedef struct Camera {
    Vector3 origin;
    Vector3 direction;
    float focal_length;
} Camera;

#endif