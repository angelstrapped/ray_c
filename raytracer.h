#ifndef RAYTRACER
#define RAYTRACER

void render_image(int canvas_width, int canvas_height, int num_channels, unsigned char *samples);

#endif
