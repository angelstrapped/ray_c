LIBS= -lm -ljpeg
OBJECTS=raytracer.o

all: clean build

clean:
	rm -f ray raytracer.o

build: objects/vector3.h objects/camera.h raytracer.o raytracer.h
	mkdir -p target
	gcc $(OBJECTS) ray.c -o target/ray $(LIBS)

raytracer.o: objects/vector3.h objects/camera.h raytracer.c
