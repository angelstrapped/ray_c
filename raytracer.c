#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include "objects/vector3.h"
#include "objects/camera.h"
#include "objects/triangle.h"
#include "objects/ray.h"
#include "objects/mesh.h"

Ray ray_from_camera(Camera cam, float x, float y);
bool find_nearest_triangle(Triangle *triangles, size_t num_triangles, Ray ray, Hit *result);

Triangle test_triangle() {
	Vector3 v0 = {
		(float)(rand() % 100) - 50.0f,
		(float)(rand() % 100) - 50.0f,
		(float)(rand() % 100) - 50.0f
	};
	Vector3 v1 = {
		(float)(rand() % 100) - 50.0f,
		(float)(rand() % 100) - 50.0f,
		(float)(rand() % 100) - 50.0f
	};
	Vector3 v2 = {
		(float)(rand() % 100) - 50.0f,
		(float)(rand() % 100) - 50.0f,
		(float)(rand() % 100) - 50.0f
	};
	Vector3 color = {
		(float)(rand() % 255),
		(float)(rand() % 255),
		(float)(rand() % 255)
	};

	return new_triangle(v0, v1, v2, color);
}

Camera test_camera() {
	Vector3 origin = {0.0f, 0.0f, -500.0f};
	Vector3 direction = {0.0f, 0.0f, 1.0f};
	float focal_length = 10.0f;

	Camera cam = {origin, direction, focal_length};
	return cam;
}

bool intersect_hierarchy(MeshHierarchy hierarchy, Ray ray, Hit *result) {
	// Intersect AABB.
	// Not sure where I found this intersection formula. I'm sure it has a name.

	// Inverse is supposed to be faster. Because multiplication is fast I guess.
	float inverse_x = 1.0 / ray.direction.x;

	float tx1 = (hierarchy.aabb_top.x - ray.origin.x) * inverse_x;
	float tx2 = (hierarchy.aabb_bottom.x - ray.origin.x) * inverse_x;

	float tx[2];
	if (tx1 > tx2) { // Swap these if this is wrong.
		tx[0] = tx2;
		tx[1] = tx1;
	} else {
		tx[0] = tx1;
		tx[1] = tx2;
	}

	float inverse_y = 1.0 / ray.direction.y;

	float ty1 = (hierarchy.aabb_top.y - ray.origin.y) * inverse_y;
	float ty2 = (hierarchy.aabb_bottom.y - ray.origin.y) * inverse_y;

	float ty[2];
	if (ty1 > ty2) { // Swap these if this is wrong.
		ty[0] = ty2;
		ty[1] = ty1;
	} else {
		ty[0] = ty1;
		ty[1] = ty2;
	}

	float inverse_z = 1.0 / ray.direction.z;

	float tz1 = (hierarchy.aabb_top.z - ray.origin.z) * inverse_z;
	float tz2 = (hierarchy.aabb_bottom.z - ray.origin.z) * inverse_z;

	float tz[2];
	if (tz1 > tz2) { // Swap these if this is wrong.
		tz[0] = tz2;
		tz[1] = tz1;
	} else {
		tz[0] = tz1;
		tz[1] = tz2;
	}

	float tmin;
	if (tx[0] > ty[0]) {
		tmin = tx[0];
	} else {
		tmin = ty[0];
	}

	if (tz[0] > tmin) {
		tmin = tz[0];
	}

	float tmax;
	if (tx[1] > ty[1]) {
		tmin = tx[1];
	} else {
		tmin = ty[1];
	}

	if (tz[1] > tmin) {
		tmin = tz[1];
	}

	if (tmax < tmin) {
		// If the AABB isn't a hit, then nothing inside it will be, either.
		return false;
	}

	if (hierarchy.child_type == e_hierarchy) {
		Hit hit;
		bool has_hit;
		bool has_mesh = false;

		Hit closest_hit;
		closest_hit.distance = -1.0;

		size_t i;
		for (i = 0; i < hierarchy.num_children; i++) {
			// This recursion means it's going to go all the way down.
			has_hit = intersect_hierarchy(hierarchy,ray, &hit);
			if (has_hit) {
				if (closest_hit.distance == -1.0) {
					closest_hit.distance = hit.distance;
					closest_hit.mesh = hit.mesh;
					has_mesh = true;
				} else if (closest_hit.distance > hit.distance) {
					closest_hit.distance = hit.distance;
					closest_hit.mesh = hit.mesh;
				}
			}
		}

		if (has_mesh) {
			*result = closest_hit;
			return has_mesh;
		}
	} else if (hierarchy.child_type == e_triangles) {
		// This is the bottom of the hierarchy.
		// Intersect children and pass the results back up the hierarchy.
		Hit hit;
		bool has_hit = false;

		has_hit = find_nearest_triangle(
			hierarchy.triangles, hierarchy.num_children, ray, &hit
		);

		*result = hit;
		return has_hit;
	}
}

bool intersect(Triangle triangle, Ray ray, Hit *result) {
	// My notes here say "Möller-Trumbore".
	float epsilon = 0.0001f;

	Vector3 h = cross(ray.direction, triangle.e2);
	float a = dot(triangle.e1, h);
	if (abs(a) < epsilon) {
		// Parallel.
		return false;
	}

	float f = 1.0f / a;
	Vector3 s = v_sub(ray.origin, triangle.v0);
	float u = f * dot(s, h);
	if ((u < 0.0) || (u > 1.0)) {
		return false;
	}

	Vector3 q = cross(s, triangle.e1);
	float v = f * dot(ray.direction, q);
	if ((v < 0.0) || (u + v > 1.0)) {
		return false;
	}

	float t = f * dot(triangle.e2, q);
	if (t > epsilon) {
		Hit hit = {t, triangle};
		*result = hit;
		return true;
	}
	return false;
}

Vector3 calculate_sky(Ray ray, Vector3 sky_color) {
	Vector3 color =  {10.0f, 12.0f, 35.0f};
	// TODO: All of it.
	return color;
}

Vector3 color_at(Triangle mesh, Vector3 intersection_point) {
	Vector3 color = mesh.color;
	// TODO: All of it.
	return color;
}

bool find_nearest_triangle(
	Triangle *triangles, size_t num_triangles, Ray ray, Hit *result
) {
	bool has_hit = false;
	bool has_mesh = false;
	Hit closest_hit;
	closest_hit.distance = -1.0;

	Hit hit;
	int i;
	for (i = 0; i < num_triangles; i++) {
		has_hit = intersect(triangles[i], ray, &hit);
		if (has_hit) {
			if (closest_hit.distance == -1.0) {
				closest_hit.distance = hit.distance;
				closest_hit.mesh = hit.mesh;
				has_mesh = true;
			} else if (closest_hit.distance > hit.distance) {
				closest_hit.distance = hit.distance;
				closest_hit.mesh = hit.mesh;
			}
		}
	}

	*result = closest_hit;
	return has_mesh;
}

bool find_nearest_mesh(Scene scene, Ray ray, Hit *result) {
	bool has_hit = false;
	bool has_mesh = false;
	Hit closest_hit;
	closest_hit.distance = -1.0;

	Hit hit;
	int i;
	for (i = 0; i < scene.num_elements; i++) {
		has_hit = intersect(scene.triangles[i], ray, &hit);
		if (has_hit) {
			if (closest_hit.distance == -1.0) {
				closest_hit.distance = hit.distance;
				closest_hit.mesh = hit.mesh;
				has_mesh = true;
			} else if (closest_hit.distance > hit.distance) {
				closest_hit.distance = hit.distance;
				closest_hit.mesh = hit.mesh;
			}
		}
	}

	*result = closest_hit;
	return has_mesh;
}

void raytrace(
	Scene scene,
	Camera cam,
	int canvas_width,
	int canvas_height,
	Vector3 sky_color,
	int num_diffuse_samples,
	int num_channels,
	unsigned char *samples
) {
	Vector3 color;

	float aspect_ratio = (float) canvas_width / (float) canvas_height;
	float x_min = -1.0;
	float x_max = 1.0;
	float y_min = -1.0 / aspect_ratio;
	float y_max = 1.0 / aspect_ratio;
	float x_step = (x_max - x_min) / (float) canvas_width;
	float y_step = (y_max - y_min) / (float) canvas_height;

	Hit hit;
	Ray ray;
	int x, y, row;
	float x_offset, y_offset;
	for (y = 0; y < canvas_height; y++) {
		y_offset = y_min + (y_step * (float) y);
		for (x = 0; x < canvas_width; x++) {
			x_offset = x_min + (x_step * (float) x);
			ray = ray_from_camera(cam, x_offset, y_offset);
			if (!find_nearest_mesh(scene, ray, &hit)) {
				color = calculate_sky(ray, sky_color);
			} else {
				Vector3 intersection_point = v_add(
					ray.origin,
					v_mul_f(ray.direction, hit.distance)
				);

				color = color_at(hit.mesh, intersection_point);

				// TODO: Diffuse samples here.
			}

			row = canvas_width * num_channels * y;
			samples[row + (x * num_channels)] = (char) ((int) color.x);
			samples[(row + (x * num_channels)) + 1] = (char) ((int) color.y);
			samples[(row + (x * num_channels)) + 2] = (char) ((int) color.z);
		}
	}
}

Ray ray_from_camera(Camera cam, float x, float y) {
	Ray new_ray;

	Vector3 pixel_offset = {-x, y, 0.0f};
	Vector3 pixel_origin = v_sub(
		v_add(cam.origin, v_mul_f(cam.direction, cam.focal_length)),
		pixel_offset
	);
	Vector3 ray_direction = normalize(v_sub(pixel_origin, cam.origin));

	new_ray.origin = cam.origin;
	new_ray.direction = ray_direction;
	return new_ray;
}

void calculate_aabb(
	Triangle *triangles, size_t num_triangles, Vector3 *top, Vector3 *bot
) {
	float x_min, x_max, y_min, y_max, z_min, z_max;

	// TODO: Owl.
}

MeshHierarchy build_hierarchy(Triangle *triangles, size_t num_triangles) {
	MeshHierarchy hierarchy = malloc(sizeof (MeshHierarchy));
	if (hierarchy == NULL) {
		exit(1);
	}

	hierarchy.child_type = e_triangles;
	hierarchy.num_children = num_triangles;
	calculate_aabb(
		triangles, num_triangles,
		*hierarchy.aabb_top, *hierarchy.aabb_bottom
	);
	hierarchy.triangles = triangles;
}

Scene build_scene() {
	size_t increment = 8;
	size_t scene_size = 0;
	size_t num_elements = 0;
	size_t element_size = sizeof (Triangle);
	Triangle *triangles;
	Scene scene;
	scene.increment = increment;
	scene.size = scene_size;
	scene.num_elements = num_elements;
	scene.element_size = element_size;
	scene.triangles = triangles;

	scene.size += scene.increment * scene.element_size;
	scene.triangles = malloc(scene.size);
	if (scene.triangles == NULL) {
		exit(1);
	}

	int toast_array_size = 3;
	Triangle toast_triangles[] = {
		new_triangle(
			(Vector3) {150.0, -50.0, 5.1},
			(Vector3) {120.0, -100.0, 5.2},
			(Vector3) {0.0, -100.0, 5.3},
			(Vector3) {155.0, 135, 235.0}
		),
		new_triangle(
			(Vector3) {250.0, -50.0, 5.4},
			(Vector3) {220.0, -100.0, 5.5},
			(Vector3) {100.0, -100.0, 5.6},
			(Vector3) {155.0, 135, 235.0}
		),
		new_triangle(
			(Vector3) {350.0, -50.0, 5.7},
			(Vector3) {320.0, -100.0, 5.8},
			(Vector3) {200.0, -100.0, 5.9},
			(Vector3) {255.0, 135, 235.0}
		),

	};

	int i;
	for (i = 0; i < 120; i++) {
		if ((scene.num_elements * scene.element_size) >= scene.size) {
			scene.size += scene.increment * scene.element_size;
			scene.triangles = realloc(scene.triangles, scene.size);
			if (scene.triangles == NULL) {
				exit(1);
			}
		}

		scene.triangles[scene.num_elements] = test_triangle();
		//scene.triangles[scene.num_elements] = toast_triangles[i];
		//printf("%f\n", scene.triangles[scene.num_elements].v0.x);
		scene.num_elements++;
	}
	return scene;
}

void render_image(
	int canvas_width, int canvas_height,
	int num_channels, unsigned char *samples
) {
	Scene scene = build_scene();

	Camera cam = test_camera();
	Vector3 sky_color = {100.0f, 100.0f, 100.0f};
	int num_diffuse_samples = 0;

	raytrace(
		scene, cam, canvas_width, canvas_height,
		sky_color, num_diffuse_samples,	num_channels, samples
	);

	free(scene.triangles);
}
