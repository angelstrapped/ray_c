#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <jpeglib.h>

#include "raytracer.h"

// Window size constants.
#define WIDTH 640
#define HEIGHT 510

// A file path is just a plain string (char *)?
void save_jpeg(int width, int height, char *samples, char *file_path) {
	printf("save\n");
	FILE *img_file = fopen(file_path, "wb");

	if (!img_file) {
		exit(1);
	}

	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);
	jpeg_stdio_dest(&cinfo, img_file);
	
	cinfo.image_width = WIDTH;
	cinfo.image_height = HEIGHT;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB; // Not a clue what JCS_RGB does.
	
	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, 75, true);
	jpeg_start_compress(&cinfo, true);
	
	// Apparently a JSAMPLE is just one channel of one pixel (e.g. 255).
	// Maybe it's char size? Maybe there's a way to check?
	// // Yes, it's unsigned char.
	// So a row of JSAMPLE is a sequence of values for successive channels.
	// E.g. for 3 channels R, G, and B:
	// RGBRGBRGBRGB
	// i.e.
	// 255255255255255255255255255255255255
	JSAMPROW row_pointer;

	int y;
	printf("Write to file: %s\n", file_path);
	for (y = 0; y < height; y++) {
		row_pointer = &samples[y * width * 3];
		jpeg_write_scanlines(&cinfo, &row_pointer, 1);
	}

	jpeg_finish_compress(&cinfo);
}

int main(int argc, char *argv[])
{	
	char *file_path = "./1.jpeg";

	int num_channels = 3;

	unsigned char *samples = malloc(HEIGHT * WIDTH * num_channels);
	if (samples == NULL) {
		exit(1);
	}

	render_image(WIDTH, HEIGHT, num_channels, samples);
	save_jpeg(WIDTH, HEIGHT, samples, file_path);
	free(samples);

	return 0;
}
