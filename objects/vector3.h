#ifndef VECTOR3
#define VECTOR3

#include <math.h>

typedef struct Vector3 {
    float x;
    float y;
    float z;
} Vector3;


/* Vector math. */

Vector3 v_sub(Vector3 a, Vector3 b) {
    Vector3 r = {
        a.x - b.x,
        a.y - b.y,
        a.z - b.z,
    };
    
    return r;
}

Vector3 v_sub_f(Vector3 a, float b) {
    Vector3 r = {
        a.x - b,
        a.y - b,
        a.z - b,
    };
    
    return r;
}

Vector3 v_add(Vector3 a, Vector3 b) {
    Vector3 r = {
        a.x + b.x,
        a.y + b.y,
        a.z + b.z,
    };
    
    return r;
}

Vector3 v_add_f(Vector3 a, float b) {
    Vector3 r = {
        a.x + b,
        a.y + b,
        a.z + b,
    };
    
    return r;
}

Vector3 v_mul(Vector3 a, Vector3 b) {
    Vector3 r = {
        a.x * b.x,
        a.y * b.y,
        a.z * b.z,
    };
    
    return r;
}

Vector3 v_mul_f(Vector3 a, float b) {
    Vector3 r = {
        a.x * b,
        a.y * b,
        a.z * b,
    };
    
    return r;
}

Vector3 v_div(Vector3 a, Vector3 b) {
    Vector3 r = {
        a.x / b.x,
        a.y / b.y,
        a.z / b.z,
    };
    
    return r;
}

Vector3 v_div_f(Vector3 a, float b) {
    Vector3 r = {
        a.x / b,
        a.y / b,
        a.z / b,
    };
    
    return r;
}

Vector3 cross(Vector3 a, Vector3 b) {
    Vector3 cross_product = {
        (a.y * b.z) - (a.z * b.y),
        (a.z * b.x) - (a.x * b.z),
        (a.x * b.y) - (a.y * b.x)
    };

    return cross_product;
}

float dot(Vector3 a, Vector3 b) {
    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}

float magnitude(Vector3 v) {
    return (float)sqrt(dot(v, v));
}

Vector3 normalize(Vector3 v) {
    float m = magnitude(v);

    Vector3 normalized = {
        v.x / m,
        v.y / m,
        v.z / m
    };
    
    return normalized;
}

#endif